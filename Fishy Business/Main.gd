extends Node2D
class_name Main

export var numberOfRounds : int = 10
export var roundDuration = 10.0

var ScoreIndicator : PackedScene

var currentRound = 0

func _enter_tree():
	if (OS.is_debug_build()):
		OS.window_fullscreen = false

func _ready():
	ScoreIndicator = preload("res://ScoreIndicator.tscn")
	#Create relevant number of level score indicators
	for n in numberOfRounds:
		var _scoreIndicator = ScoreIndicator.instance()
		_scoreIndicator.value = 0
		_scoreIndicator.max_value = roundDuration
		$Scores.add_child(_scoreIndicator)

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_F:
			OS.window_fullscreen = !OS.window_fullscreen
		if event.pressed and event.scancode == KEY_ESCAPE:
			get_tree().quit()
