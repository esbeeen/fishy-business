extends Node2D

class_name State

var fsm: StateMachine

var isActive = false

func _ready():
	hide()

func enter():
	isActive = true
	show()
	pass

func exit():
	isActive = false
	hide()

# Optional handler functions for game loop events
func process(delta):
	# Add handler code here
	return delta

func physics_process(delta):
	return delta

func input(event):
	return event

