extends State
const scorefile = "res://highscores.txt"
var main:Main

var headings = ["Sådan!", "Godt gået!", "Spillet er slut!"]

# Called when the node enters the scene tree for the first time.
func _ready():
	main = get_node("/root/Main")



func enter():
	.enter()
	
	#Calculate score
	var scoreIndicators = get_node("/root/Main/Scores").get_children()
	var userScore = 0
	for _s in scoreIndicators:
		var __s:TextureProgress = _s
		userScore = userScore + __s.value
	
	var file = File.new()
	#Create file if it doesn't exist
	if not file.file_exists(scorefile):
		file.open(scorefile, File.WRITE)
		file.close()
	
	var mode = File.READ_WRITE
	
	file.open(scorefile, File.READ_WRITE)
	
	#Read existing highscores
	var content:String = file.get_as_text()
	var _lines = content.split("\n")
	var lines =  Array(_lines)
	lines.sort()
	var scoresInAll = lines.size() 
	var worseScoresCount = 0
	for line in lines:
		if float(line) < userScore:
			worseScoresCount += 1
		else:
			print("There are " + str(worseScoresCount) + " worse scores!")
			break
		print (line)
	
	var percentage = (float(worseScoresCount) / float(scoresInAll)) * 100
	print ("Worse percentage: " + str(percentage))
		
#	print(content)
	file.seek_end();
	
	
	#Show score
	randomize()
	get_node("Endpage/Label").text = headings[ randi() % headings.size() ] #Select random heading
	get_node("Endpage/Score").text = "Du fik " + str(round( userScore )) + " point"
	get_node("Endpage/Percent").text = "Det er mere end " + str(round( percentage )) + " % af alle spillere"
	
	#Append score til highscore file
	file.store_line(str(userScore))
	file.close();
	
	main.currentRound = 0
	
	#Play sound
	$AudioStreamPlayer.play()
	
	yield(get_tree().create_timer(5), "timeout")
	fsm.change_to("Idle")


func exit():
	.exit()
	for node in get_node("/root/Main/Scores").get_children():
		node.value = 0
