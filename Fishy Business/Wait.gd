extends State


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var main:Main = get_node("/root/Main") 
onready var scores:Control = get_node("/root/Main/Scores") 
onready var countdown:TextureProgress = get_node("/root/Main/Countdown") 

# Called when the node enters the scene tree for the first time.
func _ready():

	pass


func enter():
	#Update score indicator 
	var _indicator = scores.get_child(main.currentRound) as TextureProgress
	_indicator.value = countdown.value
	_indicator.modulate = countdown.modulate
	#Increment level counter
	main.currentRound += 1
	
	#.enter()
	yield(get_tree().create_timer(2), "timeout")
	if (main.currentRound < main.numberOfRounds):
		fsm.change_to("Action")
	else:
		fsm.change_to("Finished")
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


