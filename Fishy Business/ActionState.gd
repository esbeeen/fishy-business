extends State

export var fishMinScale:float = 0.7
export var fishMaxScale:float = 1

onready var countdown:TextureProgress = get_node("/root/Main/Countdown")
onready var main:Main = get_node("/root/Main")
onready var playArea:ColorRect = get_node("PlayArea")
onready var backgrounds:Node2D = get_node("/root/Main/Backgrounds")
onready var fish:Node2D = get_node("/root/Main/Fish")

var confetti = preload("res://crab_confetti.tscn")

var currentFishbtnPosition:Vector2

#Placeholder vars to prevent current gfxs from being selected again
var backgroundIndex = 0
var fishIndex = 0

func _ready():
	pass

func process(delta):
	countdown.value -= delta
	var percentage = countdown.value / countdown.max_value
	var c = Color(Color.darkred).linear_interpolate(Color(Color.deepskyblue), percentage)
	countdown.modulate = c
	
	if (countdown.value <= 0):
		$timeout.play()
		fsm.change_to("Wait")

func enter():
	.enter()
	#Reset progress bar 
	countdown.max_value = get_node("/root/Main").roundDuration
	countdown.value = countdown.max_value
	
	for node in backgrounds.get_children():
		node.hide()
	
	for node in fish.get_children():
		node.hide()
	
#	#select random bg and random fish to show
#	#Make array of backgrounds
	var _backgrounds = backgrounds.get_children()
	#remove current bg so it cant be selected
	#Select random from list and remember it for next time
	print ("current bg ", backgroundIndex)
	backgroundIndex = selectRandomFromArray(_backgrounds, backgroundIndex)
	var currentBg = _backgrounds[backgroundIndex]
	print ("selected bg ", backgroundIndex)
	#Show it
	currentBg.show()

	#Same procedure as above
	var _fish = fish.get_children()
	#_fish.remove(fishIndex)
	fishIndex = selectRandomFromArray(_fish, fishIndex)
	var currentFish = _fish[fishIndex]
	currentFish.show()
#	
	
	#Select random position for fish
	var rect = playArea.get_global_rect()
	randomize()
	var x = rand_range(0, rect.size.x)
	randomize()
	var y = rand_range(0, rect.size.y)
	var offset = currentFish.rect_size / 2
	print (x)
	print (y)
	var pos =  Vector2(x,y) - offset;
	currentFish.set_global_position(pos) 
	currentFishbtnPosition = pos + offset
	
	#Select random rotation for fish
	randomize()
	var rotation = 360 * randf()
	print(rotation)
	currentFish.set_rotation_degrees(rotation)
	
	#select random scale for fish
	randomize()
	var scale = rand_range(fishMinScale, fishMaxScale)
	print(scale)
	currentFish.rect_scale = Vector2(scale, scale)
	
func selectRandomFromArray(arr, except):
	var index = -1
	while (index == -1 or index == except):
		randomize()
		index = randi()%arr.size()
		print(index)
	return index
	

func _on_Wrong_Guess():
	if (isActive):
		print("Wrong!")
		countdown.value -= 1
		$wrong.play()


func _on_Correct_Guess():
	if (isActive):
		print("Correct!")
		#get some confetti
		var _confetti = confetti.instance()
		print ("Fish position ", currentFishbtnPosition)
		_confetti.position = currentFishbtnPosition 
		main.add_child(_confetti)
		fsm.change_to("Wait")
		$correct.play()
